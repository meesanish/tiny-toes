# Tiny Toes

This is ecommerce site of tinytoes , built with wordpress and woocommerce

#For Setup:
Login to Wordpress Dashboard

URL: domainname.com/wp-admin
Username: admin
password: digital@7890


#For Automatic Paypal  Setup: 
Step 1: Woocommerce > Setting > Payments > Paypal Checkout > Manage
Step 2: Click on "Setup or link an existing paypal account"

#For Manual Paypal  Setup: 
Step 1: Woocommerce > Setting > Payments > Paypal Checkout > Manage
then manually enter:
    Live API Username, Live API Password
    and follow the instruction.

#Similarly for other payment option 
Woocommerce > Setting > Payments and setup.

#Add Products
Login to Wordpress Dashboard
Products > Add new and follow the instructions.

#Only wp-content folder contains our design, plugins and media files, other files are wordpress defaults.
#wp-config.php file contains connection of wordpress site to databse

#To Clone WordPress Site Manually
    Step 1 – Copy Files from Source
    Step 2 – Copy Database from Source
    Step 3 – Import Database in the Destination Site
    Step 4 – Create a new database user and give permissions
    Step 5 – Upload contents of wp-content folder
    Step 6 – Edit the wp-config file  	
    Step 6 – Change the site URL

#To Clone WordPress Site Using Duplicator Plugin
    If you want to clone WordPress website using a WordPress Site Cloner plugin, there are a handful available in the  WordPress repository. Duplicator is the most popular WordPress Site cloner plugin among them.
    Simply upload the zip file which is available on mega.


More details: https://wphow.co/kb/how-to-clone-wordpress-site/
